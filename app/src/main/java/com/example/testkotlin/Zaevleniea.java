package com.example.testkotlin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static com.google.android.gms.common.util.CollectionUtils.listOf;

public class Zaevleniea extends AppCompatActivity {

    TextView textTargetUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zaevleniea);

        String[] profList = new String[]{"43.01.09 Повар, кондитер","15.01.05 Сварщик", "54.01.20 Графический дизайнер", "23.01.17 Мастер по ремонту и обслуживанию автомобилей", "38.02.01 Экономика и бухгалтерский учет", "23.02.01 Техническое обслуживание и ремонт двигателей, систем и агрегатов автомобилей", "09.02.07 Информационные системы и программирование", "43.02.01 Организация обслуживания в общественнои питании", "32.02.01 Сестринское дело", "13.02.11 Техническая эксплуатация и обслуживание электрического и электромеханичкого оборудования", "44.02.01 Дошкольное образование", "16199 Оператор электронно-вычислительных и вычислительных мешин"};
        Spinner profSpiner = findViewById(R.id.auto_prof);
        ArrayAdapter adapterProf = new ArrayAdapter<String>(this, R.layout.prof, profList);
        profSpiner.setAdapter(adapterProf);

        textTargetUri = (TextView) findViewById(R.id.targeturi); // Потом удалить
        Button buttonLoadImageZaevlenie = findViewById(R.id.loadImageZaevlenie); // Кнопка отправки заявления
        Button buttonLoadImagePhoto = findViewById(R.id.loadImagePhoto); // Кнопка отправки Фото
        Button buttonLoadImageDiplom = findViewById(R.id.loadImageDiplom); // Кнопка отправки Диплома/Аттестата
        Button buttonSend = findViewById(R.id.btn_enter_zaevlenie); // Кнопка отправки

        final ArrayList<Uri> ImageArrayUri = new ArrayList<Uri>(); // Коллекция фоток

        buttonLoadImageZaevlenie.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) { // Обработка нажатие кнопки
                Intent inttentZaevlenie = new Intent(Intent.ACTION_GET_CONTENT,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                inttentZaevlenie.putExtra("URL_LIST", ImageArrayUri);
                startActivityForResult(inttentZaevlenie, 0);
            }
        });

        buttonLoadImagePhoto.setOnClickListener(new Button.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent inttentPhoto = new Intent(Intent.ACTION_GET_CONTENT,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                inttentPhoto.putExtra("URL_LIST", ImageArrayUri);
                startActivityForResult(inttentPhoto, 0);
            }
        });

        buttonSend.setOnClickListener(new Button.OnClickListener() { // Кнопка отправки
            @Override
            public void onClick(View v) {
                Intent intSend = new Intent(Intent.ACTION_SENDTO);

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) { // Условие что данные полученны
            Button buttonLoadImageZaevlenie = findViewById(R.id.loadImageZaevlenie); // Кнопка заявление
            buttonLoadImageZaevlenie.setBackgroundResource(R.color.greenDark); // Смена фона кнопки
            buttonLoadImageZaevlenie.setTextColor(Color.WHITE); // Смена цвета кнопки

            Uri targetUri = data.getData();
            textTargetUri.setText(targetUri.toString());
        }
    }
}