package com.example.testkotlin

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_zaevleniea.*


class MainActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

    }
    fun onTransferIntramural(view: View?) {
        val intent = Intent(this@MainActivity, timetable_intramural::class.java) // Расписание очного оделения
        startActivity(intent)
    }
    fun onTransferCalls(view: View?) {
        val intent = Intent(this@MainActivity, callsTimetable::class.java) // Расписание звонков
        startActivity(intent)
    }

    fun onTransfertZaevlenie(view: View?){
        val intent = Intent (this@MainActivity, Zaevleniea::class.java ) // Приемная комисия
        startActivity(intent)
    }

    fun onTransfertTimetable(view: View?){
        val intent = Intent (this@MainActivity, callsTimetable::class.java ) // Пока ничего
        startActivity(intent)
    }
}