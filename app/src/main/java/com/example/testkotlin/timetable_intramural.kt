package com.example.testkotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_timetable_intramural.*

class timetable_intramural : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_timetable_intramural)

        val fragmentAdapter = MyPagerAdapter(supportFragmentManager)
        viewTabs.adapter = fragmentAdapter
        tabs.setupWithViewPager(viewTabs)
    }
}
